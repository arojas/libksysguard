# translation of ksysguard_face_org.kde.ksysguard.colorgrid.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: ksysguard_face_org.kde.ksysguard.colorgrid\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-13 00:48+0000\n"
"PO-Revision-Date: 2023-03-11 18:35+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: contents/ui/Config.qml:25
#, kde-format
msgid "Use sensor color:"
msgstr "Použiť farbu senzora:"

#: contents/ui/Config.qml:30
#, kde-format
msgid "Number of Columns:"
msgstr "Počet stĺpcov:"

#: contents/ui/Config.qml:37
#, kde-format
msgctxt "@label"
msgid "Automatic"
msgstr "Automaticky"
