# translation of ksysguard_face_org.kde.ksysguard.horizontalbars.pot to Esperanto
# Copyright (C) 2024 Free Software Foundation, Inc.
# This file is distributed under the same license as the libksysguard package.
# Oliver Kellogg <olivermkellogg@gmail.com>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-08 00:39+0000\n"
"PO-Revision-Date: 2024-05-15 10:28+0100\n"
"Last-Translator: Oliver Kellogg <olivermkellogg@gmail.com>\n"
"Language-Team: Esperanto <kde-i18n-eo.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/ui/Config.qml:29
#, kde-format
msgid "Automatic Data Range"
msgstr "Aŭtomataj datumgamoj"

#: contents/ui/Config.qml:33
#, kde-format
msgid "From:"
msgstr "De:"

#: contents/ui/Config.qml:40
#, kde-format
msgid "To:"
msgstr "Al:"
